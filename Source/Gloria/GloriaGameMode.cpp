// Copyright Epic Games, Inc. All Rights Reserved.

#include "GloriaGameMode.h"
#include "GloriaCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGloriaGameMode::AGloriaGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
