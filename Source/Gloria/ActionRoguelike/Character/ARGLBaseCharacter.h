﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ARGLBaseCharacter.generated.h"

class UARGLInteractionComponent;
struct FAxis;
class UCameraComponent;
class USpringArmComponent;

UCLASS()
class GLORIA_API AARGLBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AARGLBaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	void moveForward(float value);
	void moveLeft(float value);
	void primaryAttack();
	void PrimaryInteract();

protected:
	UPROPERTY(EditAnywhere, Category="ARGL|Attack")
	TSubclassOf<AActor> ProjectileClass;

	UPROPERTY(EditAnywhere, Category="ARGL|Attack")
	UAnimMontage* AttackAnimMontage;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UARGLInteractionComponent* ARGLInteractionComponent;
};
