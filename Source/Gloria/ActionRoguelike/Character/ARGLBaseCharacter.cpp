﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ARGLBaseCharacter.h"

#include "CommonAnimTypes.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Gloria/ActionRoguelike/Components/ARGLInteractionComponent.h"


// Sets default values
AARGLBaseCharacter::AARGLBaseCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComponent = this->CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
	SpringArmComponent->SetupAttachment(this->RootComponent);
	SpringArmComponent->TargetArmLength = 400.f;
	SpringArmComponent->bUsePawnControlRotation = true;

	CameraComponent = this->CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(this->SpringArmComponent, USpringArmComponent::SocketName);

	this->bUseControllerRotationYaw = true;

	GetCharacterMovement()->bOrientRotationToMovement = true;

	ARGLInteractionComponent = this->CreateDefaultSubobject<UARGLInteractionComponent>("InteractionComponent");
}

// Called when the game starts or when spawned
void AARGLBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AARGLBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AARGLBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AARGLBaseCharacter::moveForward);
	PlayerInputComponent->BindAxis("MoveLeft", this, &AARGLBaseCharacter::moveLeft);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("PrimaryAttack", IE_Pressed, this, &AARGLBaseCharacter::primaryAttack);

	PlayerInputComponent->BindAction("PrimaryInteraction", IE_Pressed, this, &AARGLBaseCharacter::PrimaryInteract);
}

void AARGLBaseCharacter::moveForward(float value)
{
	auto controller_rotation = this->GetControlRotation();
	controller_rotation.Roll = 0.f;
	controller_rotation.Pitch = 0.f;
	AddMovementInput(controller_rotation.Vector(), value);
	//AddMovementInput(GetActorForwardVector(), value);
}

void AARGLBaseCharacter::moveLeft(float value)
{
	auto controller_rotation = this->GetControlRotation();
	controller_rotation.Roll = 0.f;
	controller_rotation.Pitch = 0.f;

	// X- Forward - Red
	// y - Right - Green
	// z - Up - Blue

	FVector right_vec = FRotationMatrix(controller_rotation).GetScaledAxis(EAxis::Y);

	AddMovementInput(right_vec, -value);
}

void AARGLBaseCharacter::primaryAttack()
{
	PlayAnimMontage(AttackAnimMontage);

	const auto hand_r_loc = GetMesh()->GetSocketLocation("hand_r");
	const auto ControllerRotation = this->GetControlRotation();

	// TODO: if CameraArm has rotation for specific view, correct the rotation that projectile spawned
	//ControllerRotaiton += SpringArmComponent->GetComponentRotation();

	FTransform SpawnTransf = FTransform(ControllerRotation, hand_r_loc);

	FActorSpawnParameters spawn_params;
	spawn_params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	GetWorld()->SpawnActor<AActor>(ProjectileClass, SpawnTransf, spawn_params);
}

void AARGLBaseCharacter::PrimaryInteract()
{
	if (this->ARGLInteractionComponent)
	{
		this->ARGLInteractionComponent->PrimaryInteract();
	}
}
