// Fill out your copyright notice in the Description page of Project Settings.


#include "ARGLInteractionComponent.h"

#include "Gloria/ActionRoguelike/Nothing/ARGLTestInterface.h"


// Sets default values for this component's properties
UARGLInteractionComponent::UARGLInteractionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UARGLInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


// Called every frame
void UARGLInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                              FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UARGLInteractionComponent::PrimaryInteract()
{
	FCollisionObjectQueryParams ObjectQueryParams;
	ObjectQueryParams.AddObjectTypesToQuery(ECC_WorldDynamic);

	AActor* Owner = GetOwner();

	FVector Start{}, End{}, EyeLocation{};
	FRotator EyeRotaion{};

	Owner->GetActorEyesViewPoint(EyeLocation, EyeRotaion);
	Start = EyeLocation;
	End = EyeLocation + EyeRotaion.Vector() * 1000.f;


	auto exectue_interacet = [](AActor* HitActor, AActor* Owner)
	{
		if (HitActor->Implements<UARGLTestInterface>())
		{
			IARGLTestInterface::Execute_Interact(HitActor, Cast<APawn>(Owner));
		}
	};

	if (auto world = GetWorld(); world)
	{
#if 0 // Sweep Trace
		const float radius = 30.f;
		TArray<FHitResult> HitResults;
		bool bBlockHit = world->SweepMultiByObjectType(HitResults, Start, End, FQuat::Identity, ObjectQueryParams,
		                                               FCollisionShape::MakeSphere(radius));

		const auto color = bBlockHit ? FColor::Green : FColor::Red;
		for (auto result : HitResults)
		{
			if (auto actor = result.GetActor(); actor)
			{
				exectue_interacet(actor, Owner);
			}
			DrawDebugSphere(world, result.ImpactPoint, radius, 32, color, false, 2.f, 0);
		}
#else //line trace
		FHitResult HitResult{};
		world->LineTraceSingleByObjectType(HitResult, Start, End, ObjectQueryParams);
		auto HitActor = HitResult.GetActor();
		if (HitActor)
		{
			exectue_interacet(HitActor, Owner);
		}
		const auto color = HitActor ? FColor::Green : FColor::Red;
#endif
		DrawDebugLine(world, EyeLocation, End, color, false, 2.f, 0, 2.f);
	}
}
