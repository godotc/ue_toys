﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ARGLMagicProjectile.h"

#include <Windows.System.Diagnostics.h>

#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Interfaces/IProjectManager.h"
#include "Particles/ParticleSystemComponent.h"


// Sets default values
AARGLMagicProjectile::AARGLMagicProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>("SphereComp");
	// SphereComponent->SetCollisionObjectType(ECC_WorldDynamic);
	// SphereComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
// SphereComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereComponent->SetCollisionProfileName("ARGL_Projectile");
	
	this->RootComponent = SphereComponent;

	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>("ParticleComp");
	ParticleSystemComponent->SetupAttachment(SphereComponent);

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("MovementComp");
	MovementComponent->InitialSpeed = 2000.f;
	MovementComponent->bRotationFollowsVelocity = true;
	MovementComponent->bInitialVelocityInLocalSpace = true;
}

// Called when the game starts or when spawned
void AARGLMagicProjectile::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AARGLMagicProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
