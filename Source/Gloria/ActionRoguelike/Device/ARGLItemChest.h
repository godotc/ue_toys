// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gloria/ActionRoguelike/Nothing/ARGLTestInterface.h"

#include "ARGLItemChest.generated.h"


UCLASS()
class GLORIA_API AARGLItemChest : public AActor, public IARGLTestInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AARGLItemChest();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void Interact_Implementation(APawn* InstigatorPawn) override;

public:
	UPROPERTY(EditAnywhere, Category= "Chest")
	FRotator TargetRotation;
	UPROPERTY(EditAnywhere, Category="Chest")
	FRotator DefaultRotation;

	UPROPERTY(EditAnywhere, Category="Chest")
	bool bSwitchable = true;

	bool bOpened = false;

protected:
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* BaseMesh;

	UPROPERTY(VisibleAnywhere)
	UChildActorComponent* LidRotateRoot;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* LidMesh;
};
