﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ARGLExplosionDevice.generated.h"

class URadialForceComponent;

UCLASS()
class GLORIA_API AARGLExplosionDevice : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AARGLExplosionDevice();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	FScriptDelegate OnMeshHitDelegate;

private:
	UFUNCTION()
	void OnMeshHit(UPrimitiveComponent* HitComponent, AActor*
	               OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
	               const FHitResult& Hit);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	URadialForceComponent* RadialForceComponent;
};
