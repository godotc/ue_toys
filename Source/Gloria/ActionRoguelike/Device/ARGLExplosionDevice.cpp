﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ARGLExplosionDevice.h"

#include <wbemcli.h>

#include "Gloria/ActionRoguelike/Projectile/ARGLMagicProjectile.h"
#include "PhysicsEngine/RadialForceComponent.h"


// Sets default values
AARGLExplosionDevice::AARGLExplosionDevice()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMeshComponent->SetSimulatePhysics(true);
	StaticMeshComponent->SetCollisionProfileName("PhysicActor");

	RootComponent = StaticMeshComponent;

	RadialForceComponent = CreateDefaultSubobject<URadialForceComponent>("RadialForce");
	RadialForceComponent->Radius = 600.f;
	RadialForceComponent->ImpulseStrength = 2000.f;
	RadialForceComponent->SetupAttachment(RootComponent);
	// FIXed not push error
	RadialForceComponent->bImpulseVelChange = true;

	this->StaticMeshComponent->OnComponentHit.AddDynamic(this, &AARGLExplosionDevice::OnMeshHit);
}

// Called when the game starts or when spawned
void AARGLExplosionDevice::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AARGLExplosionDevice::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AARGLExplosionDevice::OnMeshHit(UPrimitiveComponent* HitComponent, AActor*
                                     OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                                     const FHitResult& Hit)
{
	this->RadialForceComponent->FireImpulse();
}
