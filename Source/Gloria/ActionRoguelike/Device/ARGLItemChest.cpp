// Fill out your copyright notice in the Description page of Project Settings.


#include "ARGLItemChest.h"


// Sets default values
AARGLItemChest::AARGLItemChest()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>("BaseMesh");
	SetRootComponent(BaseMesh);

	LidRotateRoot = CreateDefaultSubobject<UChildActorComponent>("LidRotateRoot");
	LidRotateRoot->SetupAttachment(BaseMesh);

	LidMesh = CreateDefaultSubobject<UStaticMeshComponent>("LidMesh");
	LidMesh->SetupAttachment(LidRotateRoot);

	LidRotateRoot->SetRelativeRotation(DefaultRotation);
}

// Called when the game starts or when spawned
void AARGLItemChest::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AARGLItemChest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AARGLItemChest::Interact_Implementation(APawn* InstigatorPawn)
{
	IARGLTestInterface::Interact_Implementation(InstigatorPawn);

	if (bSwitchable)
	{
		if (bOpened)
		{
			LidRotateRoot->SetRelativeRotation(DefaultRotation);
			bOpened = false;
		}
		else
		{
			LidRotateRoot->SetRelativeRotation(TargetRotation);
			bOpened = true;
		}
	}
	else
	{
		LidRotateRoot->SetRelativeRotation(TargetRotation);
		bOpened = true;
	}
}
