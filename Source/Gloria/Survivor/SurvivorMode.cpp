﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SurvivorMode.h"


// Sets default values
ASurvivorMode::ASurvivorMode()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	
}

// Called when the game starts or when spawned
void ASurvivorMode::BeginPlay()
{
	Super::BeginPlay();

	
}

// Called every frame
void ASurvivorMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

