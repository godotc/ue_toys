﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SurvivorHeroCharacter.h"


// Sets default values
ASurvivorHeroCharacter::ASurvivorHeroCharacter()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASurvivorHeroCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASurvivorHeroCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASurvivorHeroCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ASurvivorHeroCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
}

