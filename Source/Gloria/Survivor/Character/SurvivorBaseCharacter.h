﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SurvivorBasePawn.h"
#include "SurvivorBaseCharacter.generated.h"

UCLASS()
class GLORIA_API ASurvivorBaseCharacter : public ASurvivorBasePawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASurvivorBaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
