﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SurvivorBasePawn.h"
#include "Components/CapsuleComponent.h"


// Sets default values
ASurvivorBasePawn::ASurvivorBasePawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASurvivorBasePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASurvivorBasePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASurvivorBasePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ASurvivorBasePawn::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
}


