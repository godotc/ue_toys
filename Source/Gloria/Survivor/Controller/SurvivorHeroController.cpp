﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SurvivorHeroController.h"


// Sets default values
ASurvivorHeroController::ASurvivorHeroController()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASurvivorHeroController::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASurvivorHeroController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

