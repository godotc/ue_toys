// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GloriaGameMode.generated.h"

UCLASS(minimalapi)
class AGloriaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGloriaGameMode();
};



